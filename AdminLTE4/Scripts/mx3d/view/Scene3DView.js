$ns("mx3d.view");

$import("lib.threejs.three", function()
{
    $import("lib.threejs.effects.AnaglyphEffect");

    $import("lib.threejs.shaders.ColorCorrectionShader");

    $import("lib.threejs.shaders.CopyShader");
    $import("lib.threejs.shaders.FXAAShader");
    $import("lib.threejs.shaders.BokehShader");
    $import("lib.threejs.shaders.DotScreenShader");
    $import("lib.threejs.shaders.HorizontalTiltShiftShader");
    $import("lib.threejs.shaders.RGBShiftShader");
    $import("lib.threejs.shaders.ShaderExtras");
    $import("lib.threejs.shaders.SSAOShader");
    $import("lib.threejs.shaders.VerticalTiltShiftShader");
    $import("lib.threejs.shaders.VignetteShader");


    $import("lib.threejs.objects.Fire");

    $import("lib.threejs.renderers.Projector");

    $import("lib.threejs.postprocessing.EffectComposer");
    $import("lib.threejs.postprocessing.BokehPass");
    $import("lib.threejs.postprocessing.MaskPass");
    $import("lib.threejs.postprocessing.RenderPass");
    $import("lib.threejs.postprocessing.ShaderPass");
    $import("lib.threejs.postprocessing.SavePass");
    $import("lib.threejs.postprocessing.OutlinePass");


    $import("lib.threejs.renderers.CSS3DRenderer");

    $import("lib.threejs.MeshLine");
     
    $import("lib.threejs.plugins.DepthPassPlugin");


    $import("lib.threejs.lines.LineSegmentsGeometry");
    $import("lib.threejs.lines.LineGeometry");
    $import("lib.threejs.lines.WireframeGeometry2");

    $import("lib.threejs.lines.LineMaterial");

    $import("lib.threejs.lines.LineSegments2");
    $import("lib.threejs.lines.Line2");
    $import("lib.threejs.lines.Wireframe");

    $import("lib.threejs.partykals");
    //$import("lib.threejs.ParticleEngine");
    //$import("lib.threejs.ParticleEngineExamples");


});

$import("mx3d.view.LabelView");

$include("mx3d.res.Scene3DView.css");

mx3d.view.Scene3DView = function () {
    var me = $extend(mx.view.View);
    //ALBERTo FRAME
    me.frame = {
        //width : window.outerWidth ,
        //height: window.outerHeight
        width: window.innerWidth,
        height: window.innerHeight
        //width: window.innerWidth - (me.cumulativeOffset(me.$element[0]).left+20),
        //height: window.innerHeight - (me.cumulativeOffset(me.$element[0]).top + 20)


    };
    var base = {};

    //ALBERTO
    me.video = null;
    me.videoImage = null;
    me.videoImageContext = null;
    me.videoTexture = null;

    //me.engine = [];
    //me.clock = null;

    me.animatedObject = [];
    me.addDemo = null;

    me.clock = null;
    me.time = 0;

    me.scene = null;

    me.sphereNewObject = null;
    me.sphereNewAlert = null;
    me.sphereNewLine = null;

    me.arrayGeometryPoints = [];
    me.contGeometryPoints = 0;
    me.geometryLine = null;

    me.camera = null;
    me.cameraParams = null;
    me.composer = null;


    me.outlinePass = null;
    me.effectFXAA = null;
    me.renderPass = null;


    //me.renderMode ="renderer";
    me.renderMode = "composer";
    me.renderer = null;
    me.rendererParams = null;
    me.mouse2 = null;
    me.clickable = false;
    me.clickableObjects = [];
    me.animateLinePath = [];


    me.newobjectindicator = false;
    me.newalertindicator = false;
    me.newlineindicator = false;

    me.anaglyphEffectEnabled = false;
    me.displayAnalyphEffectButton = false;

    me.labelViews = [];

    me.$anaglyphEffectButton = null;

    me.onobjectclick = null;
    me.onobjectdblclick = null;
    me.onobjectover = null;
    me.onmousemove = null;
    me.onaddnewobject = null;
    me.onaddnewalert = null;
    me.onaddnewline = null;


    me.onlabelviewclick = null;

    base.init = me.init;
    me.init = function(p_options)
    {
        base.init(p_options);
        me.init3D();

        me.$element.addClass("Scene3DView");
        me.$element.css("overflow", "hidden");
        me.$element.on("mouseup", _onmouseup);
        me.$element.on("mousemove", _onmousemove);
        //me.$element.on("addnewobject", _onaddnewobject);
        me.$element.on("dblclick", _ondblclick);
        me.$element.on("click", ".LabelView", _labelView_onclick);
    };



    me.cumulativeOffset = function (element) {
        var top = 0, left = 0;
        do {
            top += element.offsetTop || 0;
            left += element.offsetLeft || 0;
            element = element.offsetParent;
        } while (element);

        return {
            top: top,
            left: left
        };
    }

    me.init3D = function()
    {
        me.initScene();
        me.initCamera();
        me.initRenderer();
        me.initEffects();
        me.initObjects();
        me.initLights();
        me.initParticles();

        if (me.displayAnalyphEffectButton)
        {
            me.initAnalyEffectButton();
        }
    };

    me.initParticles = function () {


    }

    me.initScene = function()
    {
        me.scene = new THREE.Scene();
        me.scene.name = "MainScene";
    };

    me.initCamera = function()
    {
        var params = $.extend({
            fov : 45,
            aspect : me.frame.width / me.frame.height,
            near : 0.01,
            far : 10000
        }, me.cameraParams);
        me.camera = new THREE.PerspectiveCamera(params.fov, params.aspect, params.near, params.far);
        if (params.position != null)
        {
            if (isArray(params.position))
            {
                me.camera.position.fromArray(params.position);
            }
            else
            {
                var position = $.extend({
                    x : 0,
                    y : 0,
                    z : 0
                }, params.position);
                me.camera.position.copy(position);
            }
        }
        if (params.rotation != null)
        {
            if (isArray(params.rotation))
            {
                me.camera.rotation.fromArray(params.rotation);
            }
            else
            {
                var rotation = $.extend({
                    x : 0,
                    y : 0,
                    z : 0
                }, params.rotation);
                me.camera.rotation.copy(rotation);
            }
        }
        me.addObject(me.camera);
    };

    me.initRenderer = function()
    {
        var params = $.extend({
            antialias : true
        }, me.rendererParams);
        me.renderer = new THREE.WebGLRenderer(params);
        me.renderer.setSize(me.frame.width, me.frame.height);
        me.renderer.gammaInput = true;
        me.renderer.gammaOutput = true;
        me.renderer.physicallyBasedShading = true;
        me.renderer.shadowMapEnabled = true;
        me.renderer.shadowMap.enabled = true;
        me.renderer.shadowMapSoft = true;
        me.$container.append(me.renderer.domElement);
        //Alberto
        me.clock = new THREE.Clock();
        me.time = 0;

        if (me.renderMode === "composer")
        {
            me.initComposer();
        }
    };

    me.initComposer = function()
    {
        me.composer = new THREE.EffectComposer(me.renderer);
    };

    me.initEffects = function()
    {
        if (me.anaglyphEffectEnabled && me.anaglyphEffect == null)
        {
            me.anaglyphEffect = new THREE.AnaglyphEffect(me.renderer);
            me.anaglyphEffect.setSize(me.frame.width, me.frame.height);
        }
        if (me.renderMode === "composer") {
            me.renderPass = new THREE.RenderPass(me.scene, me.camera);
            me.composer.addPass(me.renderPass);

            me.outlinePass = new THREE.OutlinePass(new THREE.Vector2(window.innerWidth, window.innerHeight), me.scene, me.camera);
            me.outlinePass.pulsePeriod = 3;
            me.composer.addPass(me.outlinePass);

            me.effectFXAA = new THREE.ShaderPass(THREE.FXAAShader);
            me.effectFXAA.uniforms['resolution'].value.set(1 / window.innerWidth, 1 / window.innerHeight);
            me.effectFXAA.renderToScreen = true;
            me.composer.addPass(me.effectFXAA);
        }
    };

    me.initAnalyEffectButton = function()
    {
        me.$anaglyphEffectButton = $("<div id=analyphEffect/>");
        me.$anaglyphEffectButton.on("click", function(e)
        {
            if (e.button !== 0)
            {
                return;
            }
            e.preventDefault();
            me.anaglyphEffectEnabled = !me.anaglyphEffectEnabled;
            me.$anaglyphEffectButton.toggleClass("enabled", me.anaglyphEffectEnabled);
        });
        me.$container.append(me.$anaglyphEffectButton);
    };

    me.initObjects = function()
    {
        var imagePathNewObject = location.origin + "/Images/PinNewEntity.png";
        var textureNewObject = THREE.ImageUtils.loadTexture(imagePathNewObject);
        var material = new THREE.SpriteMaterial({ map: textureNewObject, useScreenCoordinates: true });
        me.sphereNewObject = new THREE.Sprite(material);
        var p_data = { ID: -1, TYPE: "NEWOBJECT", DESC: "New Object", STATUS: "1", DATA: "" } 
        me.sphereNewObject.name = JSON.stringify(p_data);
        me.sphereNewObject.visible = false;
        me.scene.add(me.sphereNewObject);


        var imagePathNewAlert = location.origin + "/Images/PinNewAlert.png";
        var textureNewAlert = THREE.ImageUtils.loadTexture(imagePathNewAlert);
        var material = new THREE.SpriteMaterial({ map: textureNewAlert, useScreenCoordinates: true });
        me.sphereNewAlert = new THREE.Sprite(material);
        var p_data = { ID: -2, TYPE: "NEWALERT", DESC: "New Alert", STATUS: "1", DATA: "" }
        me.sphereNewAlert.name = JSON.stringify(p_data);
        me.sphereNewAlert.visible = false;
        me.scene.add(me.sphereNewAlert);

        var imagePathNewLine = location.origin + "/Images/PinNewVertex.png";
        var textureNewLine = THREE.ImageUtils.loadTexture(imagePathNewLine);
        var material = new THREE.SpriteMaterial({ map: textureNewLine, useScreenCoordinates: true });
        me.sphereNewLine = new THREE.Sprite(material);
        var p_data = { ID: -3, TYPE: "NEWLINE", DESC: "New Line", STATUS: "1", DATA: "" }
        me.sphereNewLine.name = JSON.stringify(p_data);
        me.sphereNewLine.visible = false;
        me.scene.add(me.sphereNewLine);



        //var materialgeometryLine = new MeshLineMaterial({
        //    useMap: false,
        //    color: 0xff0000,
        //    lineWidth: 3,
        //    sizeAttenuation: false,
        //    near: me.camera.near,
        //    far: me.camera.far,
        //    opacity: 1,//params.strokes ? .5 : 1,
        //    side: THREE.DoubleSide
        //});

        //var geometry = new THREE.BufferGeometry();
        //var MAX_POINTS = 500;
        //me.arrayGeometryPoints = new Float32Array(MAX_POINTS * 3);
        //geometry.addAttribute('position', new THREE.BufferAttribute(me.arrayGeometryPoints, 3));

        //me.geometryLine = new MeshLine();
        //me.geometryLine.setGeometry(geometry);
        //me.geometryLine.geometry.addAttribute('position', new THREE.BufferAttribute(me.arrayGeometryPoints, 3));
        //var mesh = new THREE.Mesh(me.geometryLine.geometry, materialgeometryLine); // this syntax could definitely be improved!
        //mesh.name = "G_LINE";
        //mesh.visible = false;
        //me.scene.add(mesh);



        var materialgeometryLine = new THREE.LineBasicMaterial({ color: 0xff0000, linewidth: 3 });
        var geometry = new THREE.BufferGeometry();
        var MAX_POINTS = 500;
        me.arrayGeometryPoints = new Float32Array(MAX_POINTS * 3);
        geometry.addAttribute('position', new THREE.BufferAttribute(me.arrayGeometryPoints, 3));
        me.geometryLine = new THREE.Line(geometry, materialgeometryLine);
        me.geometryLine.name = "G_LINE";
        me.geometryLine.visible = false;
        me.scene.add(me.geometryLine);




    };

    me.initLights = function()
    {

    };

    me.addObject = function (p_object) {
        me.scene.add(p_object);
    };



    me.removeObject = function(p_object)
    {
        me.scene.remove(p_object);
    };

    me.addLight = function(p_light, p_helperClass)
    {
        me.scene.add(p_light);

        if (p_helperClass != null)
        {
            var helper = null;
            helper = new p_helperClass(p_light);
            me.addObject(helper);
        }
    };
    me.removeLight = function(p_light)
    {
        me.scene.remove(p_light);
    };

    me.render = function()
    {
        me.time += me.clock.getDelta()*100;
        for (var i = 0; i < me.animateLinePath.length; i++) {
            me.scene.children.find(obj => obj.uuid == me.animateLinePath[i][0]).children.find(obj => obj.uuid == me.animateLinePath[i][1]).children.find(obj => obj.uuid == me.animateLinePath[i][2]).material.uniforms.time.value = me.time;
        }
        

        var scaleVector = new THREE.Vector3()

        if (me.anaglyphEffectEnabled) {
            if (me.anaglyphEffect == null) {
                me.initEffects();
            }
            me.anaglyphEffect.render(me.scene, me.camera);
        }
        else {

            
            me.scene.traverse(function (node) {
                //               if (node instanceof THREE.Mesh) {
                //                if ((node.name == "PIN") || (node.name == "BILLBOARD")) {

                var dataNode = "";
                if (node.name.substring(0, 1) == "{")
                    dataNode = JSON.parse(node.name)
                else
                    dataNode = { ID: "", TYPE: "", DESC :"" };


                for (var i = 0; i < me.animatedObject.length; ++i) {
                    me.animatedObject[i].update();
                }
                //var dt = me.clock.getDelta();
                //for (var i = 0; i < me.engine.length; i++) {
                //    me.engine[i].update(dt * 0.5);
                //}

                if ((node.type == "Line") && (me.selectedByName)) {


                    //me.selectedByName
                    //me.selectedName

                    if (dataNode.TYPE == "DESIGN") {
                           // node.name = "ALBERTO";
                    }
                }
                //if (dataNode.TYPE == "PARTYKALS") {
                //    var scaleFactor = 200;
                //    var scale = scaleVector.subVectors(node.position, me.camera.position).length() / scaleFactor;
                //    node.scale.set(scale, scale, 1);

                //}

                if (dataNode.TYPE == "PIN") {
                    var scaleFactor = 12;
                    if (node.name.split("@")[2] == "2")
                        scaleFactor = 8;
                    var scale = scaleVector.subVectors(node.position, me.camera.position).length() / scaleFactor;
                    node.scale.set(scale, scale, 1);
                    node.visible = (!me.newobjectindicator);

                }
                if (dataNode.TYPE == "NEWOBJECT") {
                    var scaleFactor = 8;
                    var scale = scaleVector.subVectors(node.position, me.camera.position).length() / scaleFactor;
                    node.scale.set(scale, scale, 1);
                    node.visible = (me.newobjectindicator);

                }

                if (dataNode.TYPE == "NEWLINE") {
                    var scaleFactor = 8;
                    var scale = scaleVector.subVectors(node.position, me.camera.position).length() / scaleFactor;
                    node.scale.set(scale, scale, 1);
                    node.visible = (me.newlineindicator);

                }
                if (dataNode.TYPE == "NEWALERT") {
                    var scaleFactor = 8;
                    var scale = scaleVector.subVectors(node.position, me.camera.position).length() / scaleFactor;
                    node.scale.set(scale, scale, 1);
                    node.visible = (me.newalertindicator);

                }

                //                }
            });

            if (me.video != null) {
                if (me.video.readyState === me.video.HAVE_ENOUGH_DATA) {
                    me.videoImageContext.drawImage(me.video, 0, 0);
                    if (me.videoTexture)
                        me.videoTexture.needsUpdate = true;
                }
            }

            if (me.renderMode === "composer" && me.composer != null) {
                me.composer.render();
            }
            else {
                //var dt = me.clock.getDelta();
                for (var i = 0; i < me.animatedObject.length; ++i) {
                    me.animatedObject[i].update();
                }
                me.renderer.render(me.scene, me.camera);
            }
        }
    };

    me.update = function(p_forceUpdate)
    {
        me.updateLabels(p_forceUpdate);
    };

    me.updateLabels = function(p_forceUpdate)
    {
        me.labelViews.forEach(function(p_labelView)
        {
            p_labelView.update(p_forceUpdate);
        });
    };

    base.setFrame = me.setFrame;
    me.setFrame = function(p_frame)
    {
        base.setFrame(p_frame);
        if (typeof (p_frame.width) === "number" || typeof (p_frame.height) === "number")
        {
            if (me.camera != null)
            {
                me.camera.aspect = me.frame.width / me.frame.height;
                me.camera.updateProjectionMatrix();
            }

            me.update();

            if (me.renderer != null)
            {
                me.renderer.setSize(me.frame.width, me.frame.height);
            }

            if (me.anaglyphEffect != null)
            {
                me.anaglyphEffect.setSize(me.frame.width, me.frame.height);
            }
        }
    };

    var _labelProjector = null;
    me.addLabelView = function(p_options)
    {
        if (_labelProjector == null)
        {
            _labelProjector = new THREE.Projector();
            _labelProjector.frame = me.frame;
        }
        var options = $.extend({
            camera : me.camera,
            projector : _labelProjector
        }, p_options);
        var labelView = new mx3d.view.LabelView(options);
        me.addSubview(labelView);
        me.labelViews.add(labelView);
        labelView.update();
        return labelView;
    };

    me.removeLabelView = function(p_labelView)
    {
        me.removeSubview(p_labelView);
        p_labelView.camera = null;
        p_labelView.projector = null;
        me.labelViews.remove(p_labelView);
    };

    me.clearLabelViews = function()
    {
        me.labelViews.forEach(function(p_labelView)
        {
            me.removeSubview(p_labelView);
        });
    };



    function AbsolutePosition(element) {
        var top = 0, left = 0;
        do {
            top += element.offsetTop || 0;
            left += element.offsetLeft || 0;
            element = element.offsetParent;
        } while (element);

        return {
            top: top,
            left: left
        };
    }

    function realCoord()
    {
        var vec = new THREE.Vector3(); // create once and reuse
        var pos = new THREE.Vector3(); // create once and reuse

        var absolutePosition = AbsolutePosition(me.$element[0]);


        vec.set(
            (((event.clientX - absolutePosition.left) / window.innerWidth) * 2 - 1),
            -((event.clientY - absolutePosition.top) / window.innerHeight) * 2 + 1,
            0.5);

        vec.unproject(me.camera);

        vec.sub(me.camera.position).normalize();

        var distance = -me.camera.position.z / vec.z;
        var pow = Math.pow(2, me.zoomMax);

        var cord = pos.copy(me.camera.position).add(vec.multiplyScalar(distance));
        var lon = ((cord.x / pow) * 360) - 180;
        return cord;
    }



    function _onmousemove(event) {

        if (event.target !== me.renderer.domElement) {
            return;
        }


        event.preventDefault();


            // update the mouse variable
            var mouse = {
                x: 0,
                y: 0,
                z: 0
            };
        //
            var absolutePosition = AbsolutePosition(me.$element[0]);
            //mouse.x = (event.clientX / me.frame.width) * 2 - 1;
            //mouse.y = -(event.clientY / me.frame.height) * 2 + 1;



            mouse.x = (((event.clientX - absolutePosition.left) / window.innerWidth) * 2 - 1);
            mouse.y = -((event.clientY - absolutePosition.top) / window.innerHeight) * 2 + 1;


            mouse.z = 1;





            var Real = realCoord();

            // create a Ray with origin at the mouse position
            // and direction into the scene (camera direction)
            var vector = new THREE.Vector3(mouse.x, mouse.y, mouse.z);
            var projector = new THREE.Projector();
            //projector.unprojectVector(vector, me.camera);
            vector.unproject(me.camera);

            var origin = me.camera.position;
            var dir = vector.sub(me.camera.position).normalize();
            var ray = new THREE.Raycaster();
            ray.linePrecision = 50;
            ray.set(origin, dir);

            // create an array containing all objects in the scene with which
            // the ray intersects
            for (var i = 0; i < me.scene.children.find(obj => obj.type == "Object3D").children.length; i += 1) {
                //var intersects = ray.intersectObjects(me.scene.children[2].children[i].children);
                var intersects = ray.intersectObjects(me.scene.children.find(obj => obj.type == "Object3D").children[i].children);
                
                if (intersects.length > 0) {
                    var objects = intersects.map(function (p_intersect) {
                        return p_intersect.object;
                    });
                    var dataNode = JSON.parse(intersects[0].object.name);
                    if (dataNode.TYPE == "DESIGN")
                        me.selectedName = dataNode.DESC;


                    me.sphereNewAlert.visible = ((intersects[0].object.type == "Line") || (intersects[0].object.type == "MeshLine")) && (me.newalertindicator);
                    me.sphereNewAlert.position.copy(intersects[0].point);

                    me.sphereNewLine.visible = ((intersects[0].object.type == "Line") || (intersects[0].object.type == "MeshLine")) && (me.newlineindicator);
                    me.sphereNewLine.position.copy(intersects[0].point);

                    me.sphereNewObject.visible = ((intersects[0].object.type == "Line") || (intersects[0].object.type == "MeshLine")) && (me.newobjectindicator);
                    me.sphereNewObject.position.copy(intersects[0].point);
                    if ((me.contGeometryPoints !== 0) && (me.newlineindicator)) {
                        updateLine(intersects[0].point);
                    }
                    me.trigger("mousemove", {
                        idIntersect : dataNode.ID,
                        objects: objects,
                        intersects: intersects
                    });


                    //addSelectedObject(intersects[0].object);
                    if (me.renderMode === "composer") {
                        me.outlinePass.selectedObjects = intersects[0].object;
                    }

                   // me.trigger("mousemove", {});
                    return;
                }
                else
                {
                    me.selectedName = "";
                    if (me.renderMode === "composer") {
                        me.outlinePass.selectedObjects = [];
                    }
                    me.sphereNewObject.visible = false;
                    me.sphereNewLine.visible = false;
                    me.sphereNewAlert.visible = false;
                }

            }
            me.trigger("mousemove", {});
    }


    function _ondblclick(event)
    {
        if ((!me.clickable) && (!me.newobjectindicator)) {
            return;
        }

        if (event.target !== me.renderer.domElement) {
            return;
        }

        event.preventDefault();

        if (event.button === 0) {
            // update the mouse variable
            var mouse = {
                x: 0,
                y: 0,
                z: 0
            };

            var absolutePosition = AbsolutePosition(me.$element[0]);

            mouse.x = (((event.clientX - absolutePosition.left) / window.innerWidth) * 2 - 1);
            mouse.y = -((event.clientY - absolutePosition.top) / window.innerHeight) * 2 + 1;


            //mouse.x = (event.clientX / me.frame.width) * 2 - 1;
            //mouse.y = -(event.clientY / me.frame.height) * 2 + 1;

            mouse.z = 1;

            // create a Ray with origin at the mouse position
            // and direction into the scene (camera direction)
            var vector = new THREE.Vector3(mouse.x, mouse.y, mouse.z);
            var projector = new THREE.Projector();
            projector.unprojectVector(vector, me.camera);

            var origin = me.camera.position;
            var dir = vector.sub(me.camera.position).normalize();
            var ray = new THREE.Raycaster();
            ray.set(origin, dir);

            // create an array containing all objects in the scene with which
            // the ray intersects
            //ALBERTO
            if ((!me.newobjectindicator) && (!me.newlineindicator) && (!me.newalertindicator)){
                var intersects = ray.intersectObjects(me.clickableObjects);
                if (intersects.length > 0) {
                    var objects = intersects.map(function (p_intersect) {
                        return p_intersect.object;
                    });
                    me.trigger("objectdblclick", {
                        objects: objects[0],
                        intersects: intersects[0]
                    });
                }
            }
            else {

                ray.linePrecision = 50;
                ray.set(origin, dir);

                for (var i = 0; i < me.scene.children.find(obj => obj.type == "Object3D").children.length; i += 1) {
                    var intersects = ray.intersectObjects(me.scene.children.find(obj => obj.type == "Object3D").children[i].children);
                    if (intersects.length > 0) {
                        var objects = intersects.map(function (p_intersect) {
                            return p_intersect.object;
                        });
                        var dataNode = JSON.parse(intersects[0].object.name);
                        if (dataNode.TYPE == "DESIGN")
                            me.selectedName = dataNode.DESC;


                        if (me.newobjectindicator) {
                            me.trigger("addnewobject", {
                                idIntersect : dataNode.ID,
                                objects: objects,
                                intersects: intersects,
                                point: me.scale.pointToLocation(intersects[0].point)
                            });
                        }
                        if (me.newalertindicator) {
                            me.trigger("addnewalert", {
                                idIntersect: dataNode.ID,
                                objects: objects,
                                intersects: intersects,
                                point: me.scale.pointToLocation(intersects[0].point)
                            });
                        }
                        if (me.newlineindicator) {
                            me.newlineindicator = false;
                            var length = me.contGeometryPoints - 2;
                            //me.contGeometryPoints = 0
                            //me.scene.children.find(obj => obj.name == "G_LINE").geometry.setDrawRange(0, me.contGeometryPoints);
                            //me.scene.children.find(obj => obj.name == "G_LINE").visible = false; 
                            me.toolBarView.setButtonCheckState(me.toolBarView.buttons.newline.id, !me.toolBarView.buttons.newline.checked)
                            me.trigger("addnewline", {
                                geometryType :"LINE",
                                idIntersect: dataNode.ID,
                                objects: objects,
                                intersects: intersects,
                               // points: me.scale.pointsToLocations(me.arrayGeometryPoints)
                                points: me.scale.floatsToLocations(me.arrayGeometryPoints, length)

                            });
                        }
                        if (me.renderMode === "composer") {
                            me.outlinePass.selectedObjects = intersects[0].object;
                        }
                        return;
                    }
                    else {
                        me.selectedName = "";
                        if (me.renderMode === "composer") {
                            me.outlinePass.selectedObjects = [];
                        }
                        me.sphereNewObject.visible = false;
                        me.sphereNewLine.visible = false;
                        me.sphereNewAlert.visible = false;
                    }

                }


            }

        }
    }

    function _onmouseup(event)
    {
        if (!me.clickable)
        {
            return;
        }
     
        if (event.target !== me.renderer.domElement)
        {
            return;
        }

        event.preventDefault();
        if ((event.button === 0) || (event.button === 2)) {
            // update the mouse variable
            var mouse = {
                x: 0,
                y: 0,
                z: 0
            };

            var absolutePosition = AbsolutePosition(me.$element[0]);

            mouse.x = (((event.clientX - absolutePosition.left) / window.innerWidth) * 2 - 1);
            mouse.y = -((event.clientY - absolutePosition.top) / window.innerHeight) * 2 + 1;


            //mouse.x = (event.clientX / me.frame.width) * 2 - 1;
            //mouse.y = -(event.clientY / me.frame.height) * 2 + 1;

            mouse.z = 1;

            // create a Ray with origin at the mouse position
            // and direction into the scene (camera direction)
            var vector = new THREE.Vector3(mouse.x, mouse.y, mouse.z);
            var projector = new THREE.Projector();
            //projector.unprojectVector(vector, me.camera);
            vector.unproject(me.camera);

            var origin = me.camera.position;
            var dir = vector.sub(me.camera.position).normalize();
            var ray = new THREE.Raycaster();
            ray.linePrecision = 50;
            ray.set(origin, dir);

            // create an array containing all objects in the scene with which
            // the ray intersects
            if (event.button === 0) {
                if ((!me.newobjectindicator) && (!me.newalertindicator) && (!me.newlineindicator)) {
                    var intersects = ray.intersectObjects(me.clickableObjects);
                    if (intersects.length > 0) {
                        var objects = intersects.map(function (p_intersect) {
                            return p_intersect.object;
                        });

                        me.trigger("objectclick", {
                            objects: objects[0],
                            intersects: intersects[0]
                        });

                    }
                }
            }
            if ((me.newalertindicator) || (me.newlineindicator)) {
                for (var i = 0; i < me.scene.children.find(obj => obj.type == "Object3D").children.length; i += 1) {
                    //var intersects = ray.intersectObjects(me.scene.children[2].children[i].children);
                    var intersects = ray.intersectObjects(me.scene.children.find(obj => obj.type == "Object3D").children[i].children);

                    if (intersects.length > 0) {
                        var objects = intersects.map(function (p_intersect) {
                            return p_intersect.object;
                        });
                        var dataNode = JSON.parse(intersects[0].object.name);
                        if (dataNode.TYPE == "DESIGN")
                            me.selectedName = dataNode.DESC;
                        //me.trigger("addvertex", {
                        //    idIntersect: dataNode.ID,
                        //    objects: objects,
                        //    intersects: intersects
                        //});
                        if (event.button === 0) {
                            if (me.contGeometryPoints === 0) {
                                addPoint(intersects[0].point);

                            }
                            addPoint(intersects[0].point);
                        }
                        if (event.button === 2) {
                            if (me.contGeometryPoints > 0) {
                                me.contGeometryPoints = me.contGeometryPoints - 1;
                                me.scene.children.find(obj => obj.name == "G_LINE").geometry.setDrawRange(0, me.contGeometryPoints)
                                updateLine(intersects[0].point);
                            }
                        }
                        //line.geometry.setDrawRange(0, count + 1);
                        //me.arrayGeometryPoints.push(intersects[0].point);
                    }
                }
            }
        }

    }

    function addPoint(point) {
        me.arrayGeometryPoints[me.contGeometryPoints * 3 + 0] = point.x;
        me.arrayGeometryPoints[me.contGeometryPoints * 3 + 1] = point.y;
        me.arrayGeometryPoints[me.contGeometryPoints * 3 + 2] = point.z;
        //me.arrayGeometryPoints[(me.contGeometryPoints + 1) * 3 + 0] = me.arrayGeometryPoints[0];
        //me.arrayGeometryPoints[(me.contGeometryPoints + 1) * 3 + 1] = me.arrayGeometryPoints[1];
        //me.arrayGeometryPoints[(me.contGeometryPoints + 1) * 3 + 2] = me.arrayGeometryPoints[2];
        me.contGeometryPoints++;
        me.scene.children.find(obj => obj.name == "G_LINE").geometry.setDrawRange(0, me.contGeometryPoints);
        updateLine(point);
    }

    function updateLine(point) {
        me.arrayGeometryPoints[me.contGeometryPoints * 3 - 3] = point.x;
        me.arrayGeometryPoints[me.contGeometryPoints * 3 - 2] = point.y;
        me.arrayGeometryPoints[me.contGeometryPoints * 3 - 1] = point.z;
        me.scene.children.find(obj => obj.name == "G_LINE").geometry.attributes.position.needsUpdate = true;
    }

    function _labelView_onclick(e)
    {
        if (e.button === 0)
        {
            var id = e.currentTarget.id;
            me.trigger("labelviewclick", {
                labelView : me.subviews[id]
            });
        }
    }

    return me.endOfClass(arguments);
};
mx3d.view.Scene3DView.className = "mx3d.view.Scene3DView";
