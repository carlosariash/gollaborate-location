$ns("igo3d.map");

igo3d.map.MapScale = function()
{
    var me = $extend(MXObject);
    var base = {};

    me.centerLocation = {
        lon : 0,
        lat : 0
    };

    me.zoomMin = 0;
    me.zoomMax = 0;
    me.zoomInfo = null;
    me.mapProvider = null;

    base._ = me._;
    me._ = function(p_options)
    {
        if (me.canConstruct())
        {
            base._(p_options);
        }
    };


    me.locationToPoint = function (p_location) {
        var location = igo3d.util.GeoJSONUtil.normalizeLocation(p_location); // location.x location,y transforma a record
        if (location != null) {
            var centerIndex = me.zoomInfo[me.zoomMax].centerIndex;
            var centerPosition = me.zoomInfo[me.zoomMax].centerPosition;
            var indexFloat = me.mapProvider.getTileIndex(location, me.zoomMax, true);
            var lonLat = me.mapProvider.getTileLonLat(indexFloat, me.zoomMax, true);
            var xx = centerPosition.x + (indexFloat.x - centerIndex.x) * me.mapProvider.tileSize - me.mapProvider.tileSize / 2
            var yy = centerPosition.y - (indexFloat.y - centerIndex.y) * me.mapProvider.tileSize + me.mapProvider.tileSize / 2

            if (p_location.length === 3) {
                return {
                    x: centerPosition.x + (indexFloat.x - centerIndex.x) * me.mapProvider.tileSize - me.mapProvider.tileSize / 2,
                    y: centerPosition.y - (indexFloat.y - centerIndex.y) * me.mapProvider.tileSize + me.mapProvider.tileSize / 2,
                    z: p_location[2]
                };
            }
            else {
                return {
                    x: centerPosition.x + (indexFloat.x - centerIndex.x) * me.mapProvider.tileSize - me.mapProvider.tileSize / 2,
                    y: centerPosition.y - (indexFloat.y - centerIndex.y) * me.mapProvider.tileSize + me.mapProvider.tileSize / 2,
                    z: 0
                };
            }
        }
        return null;
    };
    me.floatsToLocations = function (points, length) {
        var locations = [];
        for (i = 0; i < length * 3; i = i + 3) {
            locations.push(me.pointToLocation({ x: points[i], y: points[i + 1], z: points[i + 2] }));
        }
        return locations;
    };

    me.pointsToLocations = function (points) {
        var locations = [];
        for (i = 0; i < points.length; i++) {
            locations.push(me.pointToLocation(points[i]));
        }
        return locations;
    };

    me.pointToLocation = function (point) {

        if (point != null) {
            var centerIndex = me.zoomInfo[me.zoomMax].centerIndex;
            var centerPosition = me.zoomInfo[me.zoomMax].centerPosition;
            var x = ((point.x + (me.mapProvider.tileSize / 2) - centerPosition.x)/ me.mapProvider.tileSize)+ centerIndex.x
            var y = (((point.y - (me.mapProvider.tileSize / 2) - centerPosition.y)/me.mapProvider.tileSize) - centerIndex.y)*-1
            var z = point.z
            var lonLat = me.mapProvider.getTileLonLat({ x: x, y: y, z: z }, me.zoomMax, true);
            lonLat.z = point.z;
            return lonLat;
        }
        return null;
    };


    me.locationsToPoints = function(p_locations)
    {
        var results = p_locations.map(function(p_location)
        {
            return me.locationToPoint(p_location);
        });
        return results;
    };

    me.locationToVector3 = function(p_location)
    {
        var location = igo3d.util.GeoJSONUtil.normalizeLocation(p_location);
        if (location != null)
        {
            var p = me.locationToPoint(p_location);
            var vector3 = new THREE.Vector3(p.x, p.y, p.z);
            return vector3;
        }
        return null;
    };
   
    me.vector3Tolocation = function (p_vector) {
        if (p_vector != null) {
            var l = me.pointToLocation(p_vector);
            return l;
        }
        return null;
    };


    me.locationsToPath = function(p_locations)
    {
        var results = p_locations.map(function(p_location)
        {
            return me.locationToVector3(p_location);
        });
        return results;
    };

    me.locationsToLine = function(p_locations)
    {
        var points = me.locationsToPath(p_locations);
        var geometry = new THREE.Geometry();
        geometry.vertices.addAll(points);
        var line = new THREE.Line(geometry);
        return line;
    };

    me.locationsToSplinePath = function(p_locations, p_count)
    {
        var points = me.locationsToPath(p_locations);
        var splineCurve3 = new THREE.SplineCurve3(points);
        points = splineCurve3.getPoints(p_count);
        return points;
    };

    me.locationsToSpline = function(p_locations, p_count)
    {
        var points = me.locationsToSplinePath(p_locations, p_count);

        var geometry = new THREE.Geometry();
        geometry.vertices.addAll(points);
        var line = new THREE.Line(geometry);
        return line;
    };

    return me.endOfClass(arguments);
};
igo3d.map.MapScale.className = "igo3d.map.MapScale";
