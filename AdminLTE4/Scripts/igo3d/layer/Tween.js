﻿$ns("igo3d.layer");

$import("mx3d.view.MXComponent3D");

igo3d.layer.Tween = function ()
{
    var me = $extend(mx3d.view.MXComponent3D);
    var base = {};


    base.init = me.init;
    me.init = function (p_options) {
        base.init(p_options);
    }
 
    me.init = function (p_options) {
        base.init(p_options);
    }

    me.times = null;
    me.values = null;

    me.lerp = function (t) {
        var i = 0;
        var n = me.times.length;
        while (i < n && t > me.times[i])
            i++;
        if (i == 0) return me.values[0];
        if (i == n) return me.values[n - 1];
        var p = (t - me.times[i - 1]) / (me.times[i] - me.times[i - 1]);
        if (me.values[0] instanceof THREE.Vector3)
            return me.values[i - 1].clone().lerp(me.values[i], p);
        else // its a float
            return me.values[i - 1] + p * (me.values[i] - me.values[i - 1]);
    }
    return me.endOfClass(arguments);

};
igo3d.layer.Tween.className = "igo3d.layer.Tween";