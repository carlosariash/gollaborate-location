﻿$ns("igo3d.layer");

$import("mx3d.view.MXComponent3D");

igo3d.layer.Particle= function () {
	var me = $extend(mx3d.view.MXComponent3D);
	var base = {};

	me.position     = null;
	me.velocity     = null;
	me.acceleration = null;

	me.angle             = null;
	me.angleVelocity     = null; 
	me.angleAcceleration = null; 
	
	me.size = 16.0;

	me.color   = null;
	me.opacity = null;
			
	me.age   = null;
	me.alive = null;

	me.update = function (dt) {
		me.position.add(me.velocity.clone().multiplyScalar(dt));
		me.velocity.add(me.acceleration.clone().multiplyScalar(dt));

		// convert from degrees to radians: 0.01745329251 = Math.PI/180
		me.angle += me.angleVelocity * 0.01745329251 * dt;
		me.angleVelocity += me.angleAcceleration * 0.01745329251 * dt;

		me.age += dt;

		// if the tween for a given attribute is nonempty,
		//  then use it to update the attribute's value

		if (me.sizeTween.times.length > 0)
			me.size = me.sizeTween.lerp(me.age);

		if (me.colorTween.times.length > 0) {
			var colorHSL = me.colorTween.lerp(me.age);
			me.color = new THREE.Color().setHSL(colorHSL.x, colorHSL.y, colorHSL.z);
		}

		if (me.opacityTween.times.length > 0)
			me.opacity = me.opacityTween.lerp(me.age);
	}

	base.init = me.init;

	me.init = function (p_options) {
		base.init(p_options);


		me.position = new THREE.Vector3();
		me.velocity = new THREE.Vector3(); // units per second
		me.acceleration = new THREE.Vector3();

		me.angle = 0;
		me.angleVelocity = 0; // degrees per second
		me.angleAcceleration = 0; // degrees per second, per second

		me.size = 16.0;

		me.color = new THREE.Color();
		me.opacity = 1.0;

		me.age = 0;
		me.alive = 0; // use float instead of boolean for shader purposes	



	};

	return me.endOfClass(arguments);


};
igo3d.layer.Particle.className = "igo3d.layer.Particle";
