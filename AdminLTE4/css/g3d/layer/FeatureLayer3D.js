$ns("g3d.layer");

$import("g3d.layer.Layer3D");

g3d.layer.FeatureLayer3D = function()
{
    var me = $extend(g3d.layer.Layer3D);
    var base = {};
     me.color = 0xffffff;
     var Type = Object.freeze({ "CUBE": 1, "SPHERE": 2 });

    base.init = me.init;
    me.init = function(p_options)
    {
        base.init(p_options);
    };
    
    me.addFire = function(p_coordinate)
    {

        ParticleEngine();
        engine.setValues(Examples.fountain);
        engine.initialize()
        me.parentView.particleEngine.setValues({
            positionStyle: Type.CUBE,
            positionBase: new THREE.Vector3(0, 0, 0),
            positionSpread: new THREE.Vector3(500, 0, 500),

            velocityStyle: Type.CUBE,
            velocityBase: new THREE.Vector3(0, -60, 0),
            velocitySpread: new THREE.Vector3(50, 20, 50),
            accelerationBase: new THREE.Vector3(0, -10, 0),

            angleBase: 0,
            angleSpread: 720,
            angleVelocityBase: 0,
            angleVelocitySpread: 60,

            particleTexture: THREE.ImageUtils.loadTexture('../images/snowflake.png'),

            sizeTween: new g3d.layer.Tween([0, 0.25], [1, 10]),
            colorBase: new THREE.Vector3(0.66, 1.0, 0.9), // H,S,L
            opacityTween: new g3d.layer.Tween([2, 3], [0.8, 0]),

            particlesPerSecond: 200,
            particleDeathAge: 4.0,
            emitterDeathAge: 60
        });
        var mesh = me.parentView.particleEngine.getMesh();
        var position = me.translatePoint(p_coordinate);
        mesh.position.copy(position);
        mesh.scale.set(64, 64, 1.0); // imageWidth, imageHeight

        me.addObject(mesh);
        return mesh;

    }

    me.addPin2 = function (p_coordinate, image)
    {

        var position = me.translatePoint(p_coordinate);
        var pin = THREE.ImageUtils.loadTexture(image);
        var marker = new THREE.SpriteMaterial({ map: pin });
        var sprite = new THREE.Sprite(marker);
        sprite.position.copy(position);
           
        me.parentView.camera
        //sprite.scale.set(0.2, 0.6, 0.5);
        sprite.scale.set(100, 50, 1.0);
        me.addObject(sprite);
        return sprite;

    }

    me.hideObject = function (p_object)
    {
        p_object.traverse(function (child) {
           // if (child instanceof THREE.Mesh) {
                child.visible = false;
           // }
        });
    };

    me.hideObjectById = function (id) {
        var p_object = me.parentView.scene.getObjectById(id, true)
        if (p_object === undefined)
            return
        else me.hideObject(p_object);
    };

    me.showObject = function (p_object) {
        p_object.traverse(function (child) {
           // if (child instanceof THREE.Mesh) {
                child.visible = true;
           // }
        });
    };

    me.showObjectById = function (id) {
        var p_object = me.parentView.scene.getObjectById(id, true)
        if (p_object === undefined)
            return
        else me.showObject(p_object);
    };

    me.getObjectById = function (id)
    {
        var p_object = me.parentView.scene.getObjectById(id, true)
        if (p_object === undefined)
            return null
        else  return p_object


    }

    me.deleteObject  = function(p_object)
    {
        me.removeObject(p_object);
    };

    me.AddText = function (message, p_coordinate)
    {

        var position = me.translatePoint(p_coordinate);
        me.parentView.addLabelView({
            position: position,
            text: message,
            fontSize: 8
        });
    }
   

    me.addVideo= function (p_coordinate, p_source, descripcion)
    {
        var position = me.translatePoint(p_coordinate);

        me.parentView.video = document.createElement('video');
        me.parentView.video.loop = true;
        // video.id = 'video';
        // video.type = ' video/ogg; codecs="theora, vorbis" ';
        me.parentView.video.src = p_source;
        me.parentView.video.load(); // must call after setting/changing source
        me.parentView.video.play();

        // alternative method -- 
        // create DIV in HTML:
        // <video id="myVideo" autoplay style="display:none">
        //		<source src="videos/sintel.ogv" type='video/ogg; codecs="theora, vorbis"'>
        // </video>
        // and set JS variable:
        // video = document.getElementById( 'myVideo' );

        me.parentView.videoImage = document.createElement('canvas');
        me.parentView.videoImage.width = 480;
        me.parentView.videoImage.height = 204;
        me.parentView.videoImageContext = me.parentView.videoImage.getContext('2d');
        // background color if no video present
        me.parentView.videoImageContext.fillStyle = '#000000';
        me.parentView.videoImageContext.fillRect(0, 0, me.parentView.videoImage.width, me.parentView.videoImage.height);
        me.parentView.videoTexture = new THREE.Texture(me.parentView.videoImage);
        me.parentView.videoTexture.minFilter = THREE.LinearFilter;
        me.parentView.videoTexture.magFilter = THREE.LinearFilter;

        var movieMaterial = new THREE.MeshBasicMaterial({ map: me.parentView.videoTexture, overdraw: true, side: THREE.DoubleSide });
        // the geometry on which the movie will be displayed;
        // 		movie image will be scaled to fit these dimensions.


        var spriteMaterial = new THREE.SpriteMaterial({ map: me.parentView.videoTexture,  overdraw: true, side: THREE.DoubleSide });
        var sprite = new THREE.Sprite(spriteMaterial);
        sprite.scale.set(480, 200, 1);
        sprite.name = descripcion;
        me.addObject(sprite);
        return sprite;


        //var movieGeometry = new THREE.PlaneGeometry(480, 320, 4, 4);
        //var movieScreen = new THREE.Mesh(movieGeometry, movieMaterial);
        //movieScreen.position.copy(position);
        //movieMaterial.name = descripcion;
        //me.addObject(movieScreen);
        //return movieScreen;
    };


    me.addPin = function (p_coordinate, imagePath, clickable, id)
    {

        var position = me.translatePoint(p_coordinate);
        var texture = THREE.ImageUtils.loadTexture(imagePath);
        var material = new THREE.SpriteMaterial({ map: texture, useScreenCoordinates: true });
        var sprite = new THREE.Sprite(material);
        sprite.position.copy(position);
        //sprite.position = position;

        sprite.name = "PIN";
        sprite.id = id;
        sprite.scale.set(64, 64, 1.0); // imageWidth, imageHeight
        me.addObject(sprite);
        if (clickable)  me.parentView.clickableObjects.add(sprite);
        return sprite;
    }

    me.roundRect = function (ctx, x, y, w, h, r) {
        ctx.beginPath();
        ctx.moveTo(x + r, y);
        ctx.lineTo(x + w - r, y);
        ctx.quadraticCurveTo(x + w, y, x + w, y + r);
        ctx.lineTo(x + w, y + h - r);
        ctx.quadraticCurveTo(x + w, y + h, x + w - r, y + h);
        ctx.lineTo(x + r, y + h);
        ctx.quadraticCurveTo(x, y + h, x, y + h - r);
        ctx.lineTo(x, y + r);
        ctx.quadraticCurveTo(x, y, x + r, y);
        ctx.closePath();
        ctx.fill();
        ctx.stroke();
    }

    me.AddMakeTextSprite = function (message, parameters, p_coordinate) {



        if (parameters === undefined) parameters = {};

        var fontface = parameters.hasOwnProperty("fontface") ?
            parameters["fontface"] : "Arial";

        var fontsize = parameters.hasOwnProperty("fontsize") ?
            parameters["fontsize"] : 18;

        var borderThickness = parameters.hasOwnProperty("borderThickness") ?
            parameters["borderThickness"] : 4;

        var borderColor = parameters.hasOwnProperty("borderColor") ?
            parameters["borderColor"] : { r: 0, g: 0, b: 0, a: 1.0 };

        var backgroundColor = parameters.hasOwnProperty("backgroundColor") ?
            parameters["backgroundColor"] : { r: 255, g: 255, b: 255, a: 1.0 };
        //var spriteAlignment = THREE.SpriteAlignment.topLeft;

        var canvas = document.createElement('canvas');
        canvas.width = 1000;
        var context = canvas.getContext('2d');
        context.font = "Bold " + fontsize + "px " + fontface;

        // get size data (height depends only on font size)
        var metrics = context.measureText(message);
        var textWidth = metrics.width;

        // background color
        context.fillStyle = "rgba(" + backgroundColor.r + "," + backgroundColor.g + ","
                                      + backgroundColor.b + "," + backgroundColor.a + ")";
        // border color
        context.strokeStyle = "rgba(" + borderColor.r + "," + borderColor.g + ","
                                      + borderColor.b + "," + borderColor.a + ")";
        context.lineWidth = borderThickness;
        me.roundRect(context, borderThickness / 2, borderThickness / 2, (textWidth) + borderThickness, fontsize * 1.4 + borderThickness, 6);
        // 1.4 is extra height factor for text below baseline: g,j,p,q.

        // text color
        context.fillStyle = "rgba(255, 255, 255, 1.0)";
        context.fillText(message, borderThickness, fontsize + borderThickness);

        // canvas contents will be used for a texture
        var texture = new THREE.Texture(canvas)
        texture.needsUpdate = true;
        var spriteMaterial = new THREE.SpriteMaterial(
            { map: texture, useScreenCoordinates: false });
        //var spriteMaterial = new THREE.SpriteMaterial(
        //    { map: texture, useScreenCoordinates: false, alignment: spriteAlignment });
        var position = me.translatePoint(p_coordinate);

        var sprite = new THREE.Sprite(spriteMaterial);
        sprite.scale.set(100, 50, 1.0);
        sprite.position.copy(position);
        me.addObject(sprite);
        return sprite;

    }

    me.addGeometryObject = function (WKXgeometry, p_material, cota, descripcion, Cota0)
    {

        if (WKXgeometry.WKBType == "LINESTRING")
        {
            var p_coordinates= [[]];
            for (var p = 0; p < WKXgeometry.points.length; p++) {
                p_coordinates.push([WKXgeometry.points[p].x, WKXgeometry.points[p].y, cota + (Cota0 * -1)])

            }
            p_coordinates.shift();
            return me.addLineString(p_coordinates, p_material, descripcion);

        }

    }

    me.addCircle = function(p_coordinate, p_radius, p_material)
    {
        if (!isNumber(p_radius))
        {
            throw new Error("p_radius must be a number.");
        }

        var geometry = new THREE.CircleGeometry(p_radius ? p_radius : 2);

        var material = me.translateMaterial(p_material, THREE.MeshBasicMaterial);
        var mesh = new THREE.Mesh(geometry, material);
        var position = me.translatePoint(p_coordinate);
        mesh.position.copy(position);
        me.addObject(mesh);
        return mesh;
    };

    me.addPolygon = function(p_coordinates, p_height, p_material)
    {
        if (!isNumber(p_height))
        {
            throw new Error("p_height must be a number.");
        }

        var height = p_height ? p_height : 1;
        var points = me.translatePoints(p_coordinates);
        var shape = null;
        if (points.length > 2)
        {
            shape = new THREE.Shape(points);
            if (shape.curves.length <= 1)
            {
                return null;
            }
        }
        else
        {
            return null;
        }
        var geometry = new THREE.ExtrudeGeometry(shape, {
            bevelEnabled : false,
            amount : height
        });

        var material = me.translateMaterial(p_material, THREE.MeshBasicMaterial);
        var mesh = new THREE.Mesh(geometry, material);
        mesh.position.z = -height / 2;
        me.addObject(mesh);
        return mesh;
    };
    
    me.addSpline = function(p_spline, p_pointCount, p_material)
    {
        var points = p_spline.getPoints(p_pointCount);
        return me.addLineString(points, p_material);
    };

    me.addLineString = function(p_coordinates, p_material, p_description)
    {
        var lineGeometry = new THREE.Geometry();
        var vertices = me.translatePoints(p_coordinates);
        lineGeometry.vertices.addAll(vertices);

        var material = me.translateMaterial(p_material, THREE.LineBasicMaterial);
        var line = new THREE.Line(lineGeometry, material);
        line.name = p_description;
        me.addObject(line);
        return line;
    };

    me.addPolyline = function(p_coordinates, p_material)
    {
        var coordinates = p_coordinates.clone();
        coordinates.add(p_coordinates[0]);
        return me.addLineString(coordinates, p_material);
    };

    me.translatePoint = function(p_coordinate)
    {
        var isCoordinateNumber = isNumber(p_coordinate.lon) && isNumber(p_coordinate.Lat);
        var isCoordinateArray = isArray(p_coordinate) && ((p_coordinate.length === 2) || (p_coordinate.length === 3));
        //var isCoordinateArray3D = isArray(p_coordinate) && p_coordinate.length === 3;

        var isCoordinatesNumber = isNumber(p_coordinate[0]) && isNumber(p_coordinate[1]);
        //var isCoordinatesNumber3D = isNumber(p_coordinate[0]) && isNumber(p_coordinate[1]) && isNumber(p_coordinate[2]);

        if (isNumber(p_coordinate.x) && isNumber(p_coordinate.y))
        {
            if (p_coordinate.z == null)
            {
                p_coordinate.z = 0;
            }
            if (isPlainObject(p_coordinate) || p_coordinate.constructor === THREE.Vector2)
            {
                return new THREE.Vector3(p_coordinate.x, p_coordinate.y, p_coordinate.z);
            }
            else
            {
                return p_coordinate;
            }
        }
        else if (isCoordinateNumber || isCoordinateArray && isCoordinatesNumber)
        {
            var v = me.scale.locationToVector3(p_coordinate);
            return v;
        }
        else
        {
            throw new Error(p_coordinate + " is not a validate format of coordinate. A coordinate could be a LonLat, Vector2 or Vector3.");
        }
    };

    me.translatePoints = function(p_coordinates)
    {
        return p_coordinates.map(function(p_coordinate)
        {
            return me.translatePoint(p_coordinate);
        });
    };

    me.transformAngleToEuler = function (x, y, z)
    {
        var DEG2RAD = 0.017453292519943295;
        return new THREE.Euler(x * DEG2RAD, y * DEG2RAD, z * DEG2RAD, THREE.Euler.DefaultOrder);

    }

    me.translateMaterial = function(p_material, p_materialClass, p_defaultParams)
    {
        var material = null;
        if (notEmpty(p_material))
        {
            if (isPlainObject(p_material))
            {
                var defaultParams = $.extend({
                    color : me.color
                }, p_defaultParams);
                var materialParams = $.extend(defaultParams, p_material);
                material = new p_materialClass(materialParams);
            }
            else if (isObject(p_material))
            {
                material = p_material;
            }
            else
            {
                throw new Error("p_material '" + p_material + "' can not be translated into Material.");
            }
        }
        else
        {
            throw new Error("p_material can not be null or empty");
        }
        return material;
    };

    return me.endOfClass(arguments);
};
g3d.layer.FeatureLayer3D.className = "g3d.layer.FeatureLayer3D";
