﻿$ns("g3d.layer");

$import("mx3d.view.MXComponent3D");
$import("g3d.layer.Tween");
$import("g3d.layer.Particle");




particleVertexShader = [
							"attribute vec3  customColor;",
							"attribute float customOpacity;",
							"attribute float customSize;",
							"attribute float customAngle;",
							"attribute float customVisible;",  // float used as boolean (0 = false, 1 = true)
							"varying vec4  vColor;",
							"varying float vAngle;",
							"void main()",
							"{",
								"if ( customVisible > 0.5 )", 				// true
									"vColor = vec4( customColor, customOpacity );", //     set color associated to vertex; use later in fragment shader.
								"else",							// false
									"vColor = vec4(0.0, 0.0, 0.0, 0.0);", 		//     make particle invisible.

								"vAngle = customAngle;",

								"vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );",
								"gl_PointSize = customSize * ( 300.0 / length( mvPosition.xyz ) );",     // scale particles as objects in 3D space
								"gl_Position = projectionMatrix * mvPosition;",
							"}"
].join("\n");

particleFragmentShader = [
							"uniform sampler2D texture;",
							"varying vec4 vColor;",
							"varying float vAngle;",
							"void main()",
							"{",
								"gl_FragColor = vColor;",

								"float c = cos(vAngle);",
								"float s = sin(vAngle);",
								"vec2 rotatedUV = vec2(c * (gl_PointCoord.x - 0.5) + s * (gl_PointCoord.y - 0.5) + 0.5,",
													  "c * (gl_PointCoord.y - 0.5) - s * (gl_PointCoord.x - 0.5) + 0.5);",  // rotate UV coordinates to rotate texture
									"vec4 rotatedTexture = texture2D( texture,  rotatedUV );",
								"gl_FragColor = gl_FragColor * rotatedTexture;",    // sets an otherwise white particle texture to desired color
							"}"
].join("\n");


g3d.layer.ParticleEngine = function ()
{
	var Type = Object.freeze({ "CUBE": 1, "SPHERE": 2 });




	var me = $extend(mx3d.view.MXComponent3D);
	var base = {};

	base.init = me.init;
	me.init = function (p_options) {
		base.init(p_options);
	};

	me.positionStyle = Type.CUBE;
	me.positionBase = new THREE.Vector3();
	// cube shape data
	me.positionSpread = new THREE.Vector3();
	// sphere shape data
	me.positionRadius = 0; // distance from base at which particles start

	me.velocityStyle = Type.CUBE;
	// cube movement data
	me.velocityBase = new THREE.Vector3();
	me.velocitySpread = new THREE.Vector3();
	// sphere movement data
	//   direction vector calculated using initial position
	me.speedBase = 0;
	me.speedSpread = 0;

	me.accelerationBase = new THREE.Vector3();
	me.accelerationSpread = new THREE.Vector3();

	me.angleBase = 0;
	me.angleSpread = 0;
	me.angleVelocityBase = 0;
	me.angleVelocitySpread = 0;
	me.angleAccelerationBase = 0;
	me.angleAccelerationSpread = 0;

	me.sizeBase = 0.0;
	me.sizeSpread = 0.0;
	me.sizeTween = new g3d.layer.Tween();

	// store colors in HSL format in a THREE.Vector3 object
	// http://en.wikipedia.org/wiki/HSL_and_HSV
	me.colorBase = new THREE.Vector3(0.0, 1.0, 0.5);
	me.colorSpread = new THREE.Vector3(0.0, 0.0, 0.0);
	me.colorTween = new g3d.layer.Tween();

	me.opacityBase = 1.0;
	me.opacitySpread = 0.0;
	me.opacityTween = new g3d.layer.Tween();

	me.blendStyle = THREE.NormalBlending; // false;

	me.particleArray = [];
	me.particlesPerSecond = 100;
	me.particleDeathAge = 1.0;

	////////////////////////
	// EMITTER PROPERTIES //
	////////////////////////

	me.emitterAge = 0.0;
	me.emitterAlive = true;
	me.emitterDeathAge = 60; // time (seconds) at which to stop creating particles.

	// How many particles could be active at any time?
	me.particleCount = me.particlesPerSecond * Math.min(me.particleDeathAge, me.emitterDeathAge);



	me.getMesh = function () {
		for (var i = 0; i < me.particleCount; i++) {
			// remove duplicate code somehow, here and in update function below.
			me.particleArray[i] = me.createParticle();
			me.particleGeometry.vertices[i] = me.particleArray[i].position;
			me.particleMaterial.attributes.customVisible.value[i] = me.particleArray[i].alive;
			me.particleMaterial.attributes.customColor.value[i] = me.particleArray[i].color;
			me.particleMaterial.attributes.customOpacity.value[i] = me.particleArray[i].opacity;
			me.particleMaterial.attributes.customSize.value[i] = me.particleArray[i].size;
			me.particleMaterial.attributes.customAngle.value[i] = me.particleArray[i].angle;
		}

		me.particleMaterial.blending = me.blendStyle;
		if (me.blendStyle != THREE.NormalBlending)
			me.particleMaterial.depthTest = false;

		me.particleMesh = new THREE.ParticleSystem(me.particleGeometry, me.particleMaterial);
		me.particleMesh.dynamic = true;
		me.particleMesh.sortParticles = true;
		return me.particleMesh;
	}

	//////////////
	// THREE.JS //
	//////////////




	me.particleGeometry = new THREE.Geometry();
	me.particleTexture = null;
	me.particleMaterial = new THREE.ShaderMaterial(
	{
		uniforms:
		{
			texture: { type: "t", value: me.particleTexture },
		},
		attributes:
		{
			customVisible: { type: 'f', value: [] },
			customAngle: { type: 'f', value: [] },
			customSize: { type: 'f', value: [] },
			customColor: { type: 'c', value: [] },
			customOpacity: { type: 'f', value: [] }
		},
		vertexShader: particleVertexShader,
		fragmentShader: particleFragmentShader,
		transparent: true, // alphaTest: 0.5,  // if having transparency issues, try including: alphaTest: 0.5, 
		blending: THREE.NormalBlending, depthTest: true,

	});
	me.particleMesh = new THREE.Mesh();

	me.setValues = function (parameters) {
		if (parameters === undefined) return;

		// clear any previous tweens that might exist
		me.sizeTween = new g3d.layer.Tween();
		me.colorTween = new g3d.layer.Tween();
		me.opacityTween = new g3d.layer.Tween();

		for (var key in parameters)
			this[key] = parameters[key];

		// attach tweens to particles
		//Particle.prototype.sizeTween = me.sizeTween;
		//Particle.prototype.colorTween = me.colorTween;
		//Particle.prototype.opacityTween = me.opacityTween;

		// calculate/set derived particle engine values
		me.particleArray = [];
		me.emitterAge = 0.0;
		me.emitterAlive = true;
		me.particleCount = me.particlesPerSecond * Math.min(me.particleDeathAge, me.emitterDeathAge);

		me.particleGeometry = new THREE.Geometry();
		me.particleMaterial = new THREE.ShaderMaterial(
		{
			uniforms:
			{
				texture: { type: "t", value: me.particleTexture },
			},
			attributes:
			{
				customVisible: { type: 'f', value: [] },
				customAngle: { type: 'f', value: [] },
				customSize: { type: 'f', value: [] },
				customColor: { type: 'c', value: [] },
				customOpacity: { type: 'f', value: [] }
			},
			vertexShader: particleVertexShader,
			fragmentShader: particleFragmentShader,
			transparent: true, alphaTest: 0.5, // if having transparency issues, try including: alphaTest: 0.5, 
			blending: THREE.NormalBlending, depthTest: true
		});
		me.particleMesh = new THREE.ParticleSystem();
	}

	// helper functions for randomization
	me.randomValue = function (base, spread) {
		return base + spread * (Math.random() - 0.5);
	}
	me.randomVector3 = function (base, spread) {
		var rand3 = new THREE.Vector3(Math.random() - 0.5, Math.random() - 0.5, Math.random() - 0.5);
		return new THREE.Vector3().addVectors(base, new THREE.Vector3().multiplyVectors(spread, rand3));
	}


	me.createParticle = function () {
		var particle = new g3d.layer.Particle();

		if (me.positionStyle == Type.CUBE)
			particle.position = me.randomVector3(me.positionBase, me.positionSpread);
		if (me.positionStyle == Type.SPHERE) {
			var z = 2 * Math.random() - 1;
			var t = 6.2832 * Math.random();
			var r = Math.sqrt(1 - z * z);
			var vec3 = new THREE.Vector3(r * Math.cos(t), r * Math.sin(t), z);
			particle.position = new THREE.Vector3().addVectors(me.positionBase, vec3.multiplyScalar(me.positionRadius));
		}

		if (me.velocityStyle == Type.CUBE) {
			particle.velocity = me.randomVector3(me.velocityBase, me.velocitySpread);
		}
		if (me.velocityStyle == Type.SPHERE) {
			var direction = new THREE.Vector3().subVectors(particle.position, me.positionBase);
			var speed = me.randomValue(me.speedBase, me.speedSpread);
			particle.velocity = direction.normalize().multiplyScalar(speed);
		}

		particle.acceleration = me.randomVector3(me.accelerationBase, me.accelerationSpread);

		particle.angle = me.randomValue(me.angleBase, me.angleSpread);
		particle.angleVelocity = me.randomValue(me.angleVelocityBase, me.angleVelocitySpread);
		particle.angleAcceleration = me.randomValue(me.angleAccelerationBase, me.angleAccelerationSpread);

		particle.size = me.randomValue(me.sizeBase, me.sizeSpread);

		var color = me.randomVector3(me.colorBase, me.colorSpread);
		particle.color = new THREE.Color().setHSL(color.x, color.y, color.z);

		particle.opacity = me.randomValue(me.opacityBase, me.opacitySpread);

		particle.age = 0;
		particle.alive = 0; // particles initialize as inactive
		particle.sizeTween = me.sizeTween;
		particle.colorTween = me.colorTween;
		particle.opacityTween = me.opacityTween;

		return particle;
	}

	me.update = function (dt) {
		var recycleIndices = [];

		// update particle data
		for (var i = 0; i < me.particleCount; i++) {
			if (me.particleArray[i].alive) {
				me.particleArray[i].update(dt);

				// check if particle should expire
				// could also use: death by size<0 or alpha<0.
				if (me.particleArray[i].age > me.particleDeathAge) {
					me.particleArray[i].alive = 0.0;
					recycleIndices.push(i);
				}
				// update particle properties in shader
				me.particleMaterial.attributes.customVisible.value[i] = me.particleArray[i].alive;
				me.particleMaterial.attributes.customColor.value[i] = me.particleArray[i].color;
				me.particleMaterial.attributes.customOpacity.value[i] = me.particleArray[i].opacity;
				me.particleMaterial.attributes.customSize.value[i] = me.particleArray[i].size;
				me.particleMaterial.attributes.customAngle.value[i] = me.particleArray[i].angle;
			}
		}

		// check if particle emitter is still running
		if (!me.emitterAlive) return;

		// if no particles have died yet, then there are still particles to activate
		if (me.emitterAge < me.particleDeathAge) {
			// determine indices of particles to activate
			var startIndex = Math.round(me.particlesPerSecond * (me.emitterAge + 0));
			var endIndex = Math.round(me.particlesPerSecond * (me.emitterAge + dt));
			if (endIndex > me.particleCount)
				endIndex = me.particleCount;

			for (var i = startIndex; i < endIndex; i++)
				me.particleArray[i].alive = 1.0;
		}

		// if any particles have died while the emitter is still running, we imediately recycle them
		for (var j = 0; j < recycleIndices.length; j++) {
			var i = recycleIndices[j];
			me.particleArray[i] = me.createParticle();
			me.particleArray[i].alive = 1.0; // activate right away
			me.particleGeometry.vertices[i] = me.particleArray[i].position;
		}

		// stop emitter?
		me.emitterAge += dt;
		if (me.emitterAge > me.emitterDeathAge) me.emitterAlive = false;
	}

	me.destroy = function () {
		me.scene.remove(me.particleMesh);
	}


	return me.endOfClass(arguments);
};
g3d.layer.ParticleEngine.className = "g3d.layer.ParticleEngine";
