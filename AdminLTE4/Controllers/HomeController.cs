﻿using System;
using RestSharp;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using IGO_DEMO.Models;
using IGO_DEMO;
using System.Data.Entity.Spatial;


namespace IGO_DEMO.Controllers
{
    public class HomeController : Controller
    {

        private GeoIGoEntities db = new GeoIGoEntities();

        public ActionResult AssignCoordAsset()
        {

            var client = new RestClient("https://dev.igat.cl:20443");
            var request = new RestRequest("/api/Assets/GetAllAsstes", Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", " eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9sb2NhbGl0eSI6ImVzLUNMIiwidW5pcXVlX25hbWUiOiJhZG1pbmlzdHJhZG9yIiwibmFtZWlkIjoiMSIsIkZ1bGxOYW1lIjoiQWRtaW5pc3RyYWRvciBJR08iLCJCcmFuY2hPZmZpY2UiOiIiLCJDb250cmFjdCI6IiIsIldvcmtTaGlmdElEIjoiIiwicGVybWlzc2lvbnMiOlsiVXNlckFkZFBlcm1pc3Npb24iLCJVc2VyRWRpdFBlcm1pc3Npb24iLCJVc2VyRGVsZXRlUGVybWlzc2lvbiIsIlVzZXJWaWV3UGVybWlzc2lvbiIsIlBhcmFtZXRlcnNBZGRQZXJtaXNzaW9uIiwiUGFyYW1ldGVyc0VkaXRQZXJtaXNzaW9uIiwiUGFyYW1ldGVyc0RlbGV0ZVBlcm1pc3Npb24iLCJQYXJhbWV0ZXJzVmlld1Blcm1pc3Npb24iLCJSb2xlc0FkZFBlcm1pc3Npb24iLCJSb2xlc0VkaXRQZXJtaXNzaW9uIiwiUm9sZXNEZWxldGVQZXJtaXNzaW9uIiwiUm9sZXNWaWV3UGVybWlzc2lvbiIsIkNvbXBhbnlDdXN0b21BZGRQZXJtaXNzaW9uIiwiQ29tcGFueUN1c3RvbUVkaXRQZXJtaXNzaW9uIiwiQ29tcGFueUN1c3RvbURlbGV0ZVBlcm1pc3Npb24iLCJDb21wYW55Q3VzdG9tVmlld1Blcm1pc3Npb24iLCJEZXZpY2VNYW5hZ2VtZW50QWRkUGVybWlzc2lvbiIsIkRldmljZU1hbmFnZW1lbnRFZGl0UGVybWlzc2lvbiIsIkRldmljZU1hbmFnZW1lbnREZWxldGVQZXJtaXNzaW9uIiwiRGV2aWNlTWFuYWdlbWVudFZpZXdQZXJtaXNzaW9uIiwiUGVyc29uYWxBZGRQZXJtaXNzaW9uIiwiUGVyc29uYWxFZGl0UGVybWlzc2lvbiIsIlBlcnNvbmFsRGVsZXRlUGVybWlzc2lvbiIsIlBlcnNvbmFsVmlld1Blcm1pc3Npb24iLCJXb3Jrc2hpZnRBZGRQZXJtaXNzaW9uIiwiV29ya3NoaWZ0RWRpdFBlcm1pc3Npb24iLCJXb3Jrc2hpZnREZWxldGVQZXJtaXNzaW9uIiwiV29ya3NoaWZ0Vmlld1Blcm1pc3Npb24iLCJBc3NldHNBZGRQZXJtaXNzaW9uIiwiQXNzZXRzRWRpdFBlcm1pc3Npb24iLCJBc3NldHNEZWxldGVQZXJtaXNzaW9uIiwiQXNzZXRzVmlld1Blcm1pc3Npb24iLCJQYXJ0c0FkZFBlcm1pc3Npb24iLCJQYXJ0c0VkaXRQZXJtaXNzaW9uIiwiUGFydHNEZWxldGVQZXJtaXNzaW9uIiwiUGFydHNWaWV3UGVybWlzc2lvbiIsIkN1c3RvbWVyc0FkZFBlcm1pc3Npb24iLCJDdXN0b21lcnNFZGl0UGVybWlzc2lvbiIsIkN1c3RvbWVyc0RlbGV0ZVBlcm1pc3Npb24iLCJDdXN0b21lcnNWaWV3UGVybWlzc2lvbiIsIkJyYW5jaE9mZmljZXNBZGRQZXJtaXNzaW9uIiwiQnJhbmNoT2ZmaWNlc0VkaXRQZXJtaXNzaW9uIiwiQnJhbmNoT2ZmaWNlc0RlbGV0ZVBlcm1pc3Npb24iLCJCcmFuY2hPZmZpY2VzVmlld1Blcm1pc3Npb24iLCJUZWNobmljYWxMb2NhdGlvbkFkZFBlcm1pc3Npb24iLCJUZWNobmljYWxMb2NhdGlvbkVkaXRQZXJtaXNzaW9uIiwiVGVjaG5pY2FsTG9jYXRpb25EZWxldGVQZXJtaXNzaW9uIiwiVGVjaG5pY2FsTG9jYXRpb25WaWV3UGVybWlzc2lvbiJdLCJuYmYiOjE1ODEwMDIwOTQsImV4cCI6MTU4MTAxNDA5NCwiaWF0IjoxNTgxMDAyMDk0LCJpc3MiOiJodHRwczovL2Rldi5pZ2F0LmNsOjIwMDgwIiwiYXVkIjoiaHR0cHM6Ly9kZXYuaWdhdC5jbDoyMDA4MCJ9.M1jUbhI5lNkSbRvi07xYDisEhW_GfJu-0xOFfZv6p-0");
            IRestResponse response = client.Execute(request);
            List<IgoAsset> assets = Newtonsoft.Json.JsonConvert.DeserializeObject<List<IgoAsset>>(response.Content);
            return View(assets);

        }

        //public ActionResult AssignCoordAsset(string AssetId)
        //{

        //    return Index();

        //}



        public ActionResult SelectIgoAsset(string AssetType, double lat, double lon, double z)
        {
            SelectAsset model = PopulateSelectAsset(null);
            return View(model);

        }
        public ActionResult SelectIgoAsset()
        {

            return View();

        }

        [HttpPost]
        public ActionResult SelectIgoAsset(string assetType)
        {
            SelectAsset model = PopulateSelectAsset(assetType);
            return View(model);
        }


        private static SelectAsset PopulateSelectAsset(string assetType)
        {

            var client = new RestClient("https://dev.igat.cl:20443");
            var request = new RestRequest("/api/Assets/GetAllAsstes", Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", " eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9sb2NhbGl0eSI6ImVzLUNMIiwidW5pcXVlX25hbWUiOiJhZG1pbmlzdHJhZG9yIiwibmFtZWlkIjoiMSIsIkZ1bGxOYW1lIjoiQWRtaW5pc3RyYWRvciBJR08iLCJCcmFuY2hPZmZpY2UiOiIiLCJDb250cmFjdCI6IiIsIldvcmtTaGlmdElEIjoiIiwicGVybWlzc2lvbnMiOlsiVXNlckFkZFBlcm1pc3Npb24iLCJVc2VyRWRpdFBlcm1pc3Npb24iLCJVc2VyRGVsZXRlUGVybWlzc2lvbiIsIlVzZXJWaWV3UGVybWlzc2lvbiIsIlBhcmFtZXRlcnNBZGRQZXJtaXNzaW9uIiwiUGFyYW1ldGVyc0VkaXRQZXJtaXNzaW9uIiwiUGFyYW1ldGVyc0RlbGV0ZVBlcm1pc3Npb24iLCJQYXJhbWV0ZXJzVmlld1Blcm1pc3Npb24iLCJSb2xlc0FkZFBlcm1pc3Npb24iLCJSb2xlc0VkaXRQZXJtaXNzaW9uIiwiUm9sZXNEZWxldGVQZXJtaXNzaW9uIiwiUm9sZXNWaWV3UGVybWlzc2lvbiIsIkNvbXBhbnlDdXN0b21BZGRQZXJtaXNzaW9uIiwiQ29tcGFueUN1c3RvbUVkaXRQZXJtaXNzaW9uIiwiQ29tcGFueUN1c3RvbURlbGV0ZVBlcm1pc3Npb24iLCJDb21wYW55Q3VzdG9tVmlld1Blcm1pc3Npb24iLCJEZXZpY2VNYW5hZ2VtZW50QWRkUGVybWlzc2lvbiIsIkRldmljZU1hbmFnZW1lbnRFZGl0UGVybWlzc2lvbiIsIkRldmljZU1hbmFnZW1lbnREZWxldGVQZXJtaXNzaW9uIiwiRGV2aWNlTWFuYWdlbWVudFZpZXdQZXJtaXNzaW9uIiwiUGVyc29uYWxBZGRQZXJtaXNzaW9uIiwiUGVyc29uYWxFZGl0UGVybWlzc2lvbiIsIlBlcnNvbmFsRGVsZXRlUGVybWlzc2lvbiIsIlBlcnNvbmFsVmlld1Blcm1pc3Npb24iLCJXb3Jrc2hpZnRBZGRQZXJtaXNzaW9uIiwiV29ya3NoaWZ0RWRpdFBlcm1pc3Npb24iLCJXb3Jrc2hpZnREZWxldGVQZXJtaXNzaW9uIiwiV29ya3NoaWZ0Vmlld1Blcm1pc3Npb24iLCJBc3NldHNBZGRQZXJtaXNzaW9uIiwiQXNzZXRzRWRpdFBlcm1pc3Npb24iLCJBc3NldHNEZWxldGVQZXJtaXNzaW9uIiwiQXNzZXRzVmlld1Blcm1pc3Npb24iLCJQYXJ0c0FkZFBlcm1pc3Npb24iLCJQYXJ0c0VkaXRQZXJtaXNzaW9uIiwiUGFydHNEZWxldGVQZXJtaXNzaW9uIiwiUGFydHNWaWV3UGVybWlzc2lvbiIsIkN1c3RvbWVyc0FkZFBlcm1pc3Npb24iLCJDdXN0b21lcnNFZGl0UGVybWlzc2lvbiIsIkN1c3RvbWVyc0RlbGV0ZVBlcm1pc3Npb24iLCJDdXN0b21lcnNWaWV3UGVybWlzc2lvbiIsIkJyYW5jaE9mZmljZXNBZGRQZXJtaXNzaW9uIiwiQnJhbmNoT2ZmaWNlc0VkaXRQZXJtaXNzaW9uIiwiQnJhbmNoT2ZmaWNlc0RlbGV0ZVBlcm1pc3Npb24iLCJCcmFuY2hPZmZpY2VzVmlld1Blcm1pc3Npb24iLCJUZWNobmljYWxMb2NhdGlvbkFkZFBlcm1pc3Npb24iLCJUZWNobmljYWxMb2NhdGlvbkVkaXRQZXJtaXNzaW9uIiwiVGVjaG5pY2FsTG9jYXRpb25EZWxldGVQZXJtaXNzaW9uIiwiVGVjaG5pY2FsTG9jYXRpb25WaWV3UGVybWlzc2lvbiJdLCJuYmYiOjE1ODEwMDIwOTQsImV4cCI6MTU4MTAxNDA5NCwiaWF0IjoxNTgxMDAyMDk0LCJpc3MiOiJodHRwczovL2Rldi5pZ2F0LmNsOjIwMDgwIiwiYXVkIjoiaHR0cHM6Ly9kZXYuaWdhdC5jbDoyMDA4MCJ9.M1jUbhI5lNkSbRvi07xYDisEhW_GfJu-0xOFfZv6p-0");
            IRestResponse response = client.Execute(request);
            List<IgoAsset> assets = Newtonsoft.Json.JsonConvert.DeserializeObject<List<IgoAsset>>(response.Content);


            SelectAsset model = new SelectAsset()
                {
                assets = (from c in assets
                                 where c.TypeAssets == assetType || string.IsNullOrEmpty(assetType)
                                 select c).ToList(),
                AssetTypes = (from c in assets
                              select new SelectListItem { Text = c.TypeAssets, Value = c.TypeAssets }).Distinct().ToList()
                };

                return model;
        }

        public ActionResult IgoAssetDetail(int ID)
        {
            var client = new RestClient("https://dev.igat.cl:20443");
            var request = new RestRequest("/api/Assets/GetAllAsstes", Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", " eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9sb2NhbGl0eSI6ImVzLUNMIiwidW5pcXVlX25hbWUiOiJhZG1pbmlzdHJhZG9yIiwibmFtZWlkIjoiMSIsIkZ1bGxOYW1lIjoiQWRtaW5pc3RyYWRvciBJR08iLCJCcmFuY2hPZmZpY2UiOiIiLCJDb250cmFjdCI6IiIsIldvcmtTaGlmdElEIjoiIiwicGVybWlzc2lvbnMiOlsiVXNlckFkZFBlcm1pc3Npb24iLCJVc2VyRWRpdFBlcm1pc3Npb24iLCJVc2VyRGVsZXRlUGVybWlzc2lvbiIsIlVzZXJWaWV3UGVybWlzc2lvbiIsIlBhcmFtZXRlcnNBZGRQZXJtaXNzaW9uIiwiUGFyYW1ldGVyc0VkaXRQZXJtaXNzaW9uIiwiUGFyYW1ldGVyc0RlbGV0ZVBlcm1pc3Npb24iLCJQYXJhbWV0ZXJzVmlld1Blcm1pc3Npb24iLCJSb2xlc0FkZFBlcm1pc3Npb24iLCJSb2xlc0VkaXRQZXJtaXNzaW9uIiwiUm9sZXNEZWxldGVQZXJtaXNzaW9uIiwiUm9sZXNWaWV3UGVybWlzc2lvbiIsIkNvbXBhbnlDdXN0b21BZGRQZXJtaXNzaW9uIiwiQ29tcGFueUN1c3RvbUVkaXRQZXJtaXNzaW9uIiwiQ29tcGFueUN1c3RvbURlbGV0ZVBlcm1pc3Npb24iLCJDb21wYW55Q3VzdG9tVmlld1Blcm1pc3Npb24iLCJEZXZpY2VNYW5hZ2VtZW50QWRkUGVybWlzc2lvbiIsIkRldmljZU1hbmFnZW1lbnRFZGl0UGVybWlzc2lvbiIsIkRldmljZU1hbmFnZW1lbnREZWxldGVQZXJtaXNzaW9uIiwiRGV2aWNlTWFuYWdlbWVudFZpZXdQZXJtaXNzaW9uIiwiUGVyc29uYWxBZGRQZXJtaXNzaW9uIiwiUGVyc29uYWxFZGl0UGVybWlzc2lvbiIsIlBlcnNvbmFsRGVsZXRlUGVybWlzc2lvbiIsIlBlcnNvbmFsVmlld1Blcm1pc3Npb24iLCJXb3Jrc2hpZnRBZGRQZXJtaXNzaW9uIiwiV29ya3NoaWZ0RWRpdFBlcm1pc3Npb24iLCJXb3Jrc2hpZnREZWxldGVQZXJtaXNzaW9uIiwiV29ya3NoaWZ0Vmlld1Blcm1pc3Npb24iLCJBc3NldHNBZGRQZXJtaXNzaW9uIiwiQXNzZXRzRWRpdFBlcm1pc3Npb24iLCJBc3NldHNEZWxldGVQZXJtaXNzaW9uIiwiQXNzZXRzVmlld1Blcm1pc3Npb24iLCJQYXJ0c0FkZFBlcm1pc3Npb24iLCJQYXJ0c0VkaXRQZXJtaXNzaW9uIiwiUGFydHNEZWxldGVQZXJtaXNzaW9uIiwiUGFydHNWaWV3UGVybWlzc2lvbiIsIkN1c3RvbWVyc0FkZFBlcm1pc3Npb24iLCJDdXN0b21lcnNFZGl0UGVybWlzc2lvbiIsIkN1c3RvbWVyc0RlbGV0ZVBlcm1pc3Npb24iLCJDdXN0b21lcnNWaWV3UGVybWlzc2lvbiIsIkJyYW5jaE9mZmljZXNBZGRQZXJtaXNzaW9uIiwiQnJhbmNoT2ZmaWNlc0VkaXRQZXJtaXNzaW9uIiwiQnJhbmNoT2ZmaWNlc0RlbGV0ZVBlcm1pc3Npb24iLCJCcmFuY2hPZmZpY2VzVmlld1Blcm1pc3Npb24iLCJUZWNobmljYWxMb2NhdGlvbkFkZFBlcm1pc3Npb24iLCJUZWNobmljYWxMb2NhdGlvbkVkaXRQZXJtaXNzaW9uIiwiVGVjaG5pY2FsTG9jYXRpb25EZWxldGVQZXJtaXNzaW9uIiwiVGVjaG5pY2FsTG9jYXRpb25WaWV3UGVybWlzc2lvbiJdLCJuYmYiOjE1ODEwMDIwOTQsImV4cCI6MTU4MTAxNDA5NCwiaWF0IjoxNTgxMDAyMDk0LCJpc3MiOiJodHRwczovL2Rldi5pZ2F0LmNsOjIwMDgwIiwiYXVkIjoiaHR0cHM6Ly9kZXYuaWdhdC5jbDoyMDA4MCJ9.M1jUbhI5lNkSbRvi07xYDisEhW_GfJu-0xOFfZv6p-0");
            IRestResponse response = client.Execute(request);
            List<IgoAsset> assets = Newtonsoft.Json.JsonConvert.DeserializeObject<List<IgoAsset>>(response.Content);
            IgoAsset asset = assets.Where(h => h.AssetID == ID).First();
            return View(asset);
        }
        public ActionResult AssetDetail(int ID)
        {
            Asset asset = db.Assets.First(a => a.Id == ID);

            return View(asset);
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AnotherLink()
        {
            return View("Index");
        }


        public virtual string getDisenoSectores(string codSector)
        {

            SetsDisenos resultado = new SetsDisenos();
            resultado.Centro = new Models.Point3D();
            resultado.Descripcion = "All";
            resultado.Sets = new List<Models.SetDisenos>();
            double sumCota0 = 0;
            int numCota0 = 0;
            double SX = 0;
            double SY = 0;
            //double SZ = 0;
            //foreach (SECTORES sector in db.SECTORES.ToList())
            foreach (SECTORES sector in db.SECTORES.Where(a => a.SECTOR == codSector).ToList())
            {

                List<DisenoModel> lstDisenos = new List<DisenoModel>();
                double X = 0;
                double Y = 0;
                //double Z = 0;
                foreach (DISENOS d in db.DISENOS.Where(a => a.SECTOR == sector.SECTOR).ToList())
                {
                    DisenoModel diseno = new DisenoModel();
                    diseno.DESCRIPCION = d.DESCRIPCION;
                    diseno.id = d.id;
                    diseno.NIVEL = d.NIVEL;
                    diseno.SECTOR = d.SECTOR;
                    diseno.text = d.text;
                    diseno.Ubicacion_PL = d.Ubicacion_PL;
                    diseno.Ubicacion_SC = d.Ubicacion_SC;
                    diseno.Ubicacion_UB = d.Ubicacion_UB;
                    diseno.Centro = new Models.Point3D(d.WKT_DATA.Envelope.Centroid.XCoordinate.Value, d.WKT_DATA.Envelope.Centroid.YCoordinate.Value, sector.COTA);
                    diseno.WKT_DATA = d.WKT_DATA.AsText();
                    X = X + d.WKT_DATA.Envelope.Centroid.XCoordinate.Value;
                    Y = Y + d.WKT_DATA.Envelope.Centroid.YCoordinate.Value;
                    lstDisenos.Add(diseno);
                }

                SetDisenos set = new Models.SetDisenos();
                set.Selected = sector.SECTOR.Equals(codSector);
                set.Descripcion = sector.DESCRIPCION;
                set.Cota = sector.COTA;
                set.Disenos = lstDisenos;
                set.Centro = new Point3D() { X = (X / lstDisenos.Count), Y = (Y / lstDisenos.Count) };
                SX = SX + set.Centro.X;
                SY = SY + set.Centro.Y;
                if (set.Selected)
                {
                    sumCota0 = set.Cota + sumCota0;
                    numCota0++;
                }
                resultado.Sets.Add(set);
            }
            if (resultado.Sets.Count() > 0)
            {
                resultado.Centro = new Point3D() { X = (SX / resultado.Sets.Count), Y = (SY / resultado.Sets.Count) };
                resultado.Cota0 = sumCota0 / numCota0;
            }
            return new JavaScriptSerializer().Serialize(resultado);
        }

        public virtual string GettipoSTR(int tipo)
        {
            if ((tipo == 3) || (tipo == 16)) return "Extraccion";
            if ((tipo == 30) ||  (tipo == 37) || (tipo == 482)) return "Injeccion";
            if (tipo == 4) return "Fan";
            if (tipo == 12) return "Flujometro";
            if (tipo == 11) return "SensorNivelLaser";
            return string.Empty;
        }

        public virtual bool SetAssetLocation(int AssetId, int idDesign, double lon, double lat, double z)
        {
            SECTORES sector = db.SECTORES.Where(s => s.COTA == z).First();
            WKB_UBICACION ubicacion = new IGO_DEMO.WKB_UBICACION();
            ubicacion.ESTADO = 0;
            ubicacion.ID_ACTIVO = AssetId;
            ubicacion.ID_DISENO = idDesign;
            ubicacion.SECTOR = sector.SECTOR;
            ubicacion.UBICACION = DbGeometry.FromText("POINT(" + lon.ToString() + " " + lat.ToString() + ")");
            db.WKB_UBICACION.Add(ubicacion);
            db.SaveChanges();
            return true;
        }
        public virtual string getActivosSector(string codSector)
        {
            var client = new RestClient("https://dev.igat.cl:20443");
            var request = new RestRequest("/api/Assets/GetAllAsstes", Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", " eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9sb2NhbGl0eSI6ImVzLUNMIiwidW5pcXVlX25hbWUiOiJhZG1pbmlzdHJhZG9yIiwibmFtZWlkIjoiMSIsIkZ1bGxOYW1lIjoiQWRtaW5pc3RyYWRvciBJR08iLCJCcmFuY2hPZmZpY2UiOiIiLCJDb250cmFjdCI6IiIsIldvcmtTaGlmdElEIjoiIiwicGVybWlzc2lvbnMiOlsiVXNlckFkZFBlcm1pc3Npb24iLCJVc2VyRWRpdFBlcm1pc3Npb24iLCJVc2VyRGVsZXRlUGVybWlzc2lvbiIsIlVzZXJWaWV3UGVybWlzc2lvbiIsIlBhcmFtZXRlcnNBZGRQZXJtaXNzaW9uIiwiUGFyYW1ldGVyc0VkaXRQZXJtaXNzaW9uIiwiUGFyYW1ldGVyc0RlbGV0ZVBlcm1pc3Npb24iLCJQYXJhbWV0ZXJzVmlld1Blcm1pc3Npb24iLCJSb2xlc0FkZFBlcm1pc3Npb24iLCJSb2xlc0VkaXRQZXJtaXNzaW9uIiwiUm9sZXNEZWxldGVQZXJtaXNzaW9uIiwiUm9sZXNWaWV3UGVybWlzc2lvbiIsIkNvbXBhbnlDdXN0b21BZGRQZXJtaXNzaW9uIiwiQ29tcGFueUN1c3RvbUVkaXRQZXJtaXNzaW9uIiwiQ29tcGFueUN1c3RvbURlbGV0ZVBlcm1pc3Npb24iLCJDb21wYW55Q3VzdG9tVmlld1Blcm1pc3Npb24iLCJEZXZpY2VNYW5hZ2VtZW50QWRkUGVybWlzc2lvbiIsIkRldmljZU1hbmFnZW1lbnRFZGl0UGVybWlzc2lvbiIsIkRldmljZU1hbmFnZW1lbnREZWxldGVQZXJtaXNzaW9uIiwiRGV2aWNlTWFuYWdlbWVudFZpZXdQZXJtaXNzaW9uIiwiUGVyc29uYWxBZGRQZXJtaXNzaW9uIiwiUGVyc29uYWxFZGl0UGVybWlzc2lvbiIsIlBlcnNvbmFsRGVsZXRlUGVybWlzc2lvbiIsIlBlcnNvbmFsVmlld1Blcm1pc3Npb24iLCJXb3Jrc2hpZnRBZGRQZXJtaXNzaW9uIiwiV29ya3NoaWZ0RWRpdFBlcm1pc3Npb24iLCJXb3Jrc2hpZnREZWxldGVQZXJtaXNzaW9uIiwiV29ya3NoaWZ0Vmlld1Blcm1pc3Npb24iLCJBc3NldHNBZGRQZXJtaXNzaW9uIiwiQXNzZXRzRWRpdFBlcm1pc3Npb24iLCJBc3NldHNEZWxldGVQZXJtaXNzaW9uIiwiQXNzZXRzVmlld1Blcm1pc3Npb24iLCJQYXJ0c0FkZFBlcm1pc3Npb24iLCJQYXJ0c0VkaXRQZXJtaXNzaW9uIiwiUGFydHNEZWxldGVQZXJtaXNzaW9uIiwiUGFydHNWaWV3UGVybWlzc2lvbiIsIkN1c3RvbWVyc0FkZFBlcm1pc3Npb24iLCJDdXN0b21lcnNFZGl0UGVybWlzc2lvbiIsIkN1c3RvbWVyc0RlbGV0ZVBlcm1pc3Npb24iLCJDdXN0b21lcnNWaWV3UGVybWlzc2lvbiIsIkJyYW5jaE9mZmljZXNBZGRQZXJtaXNzaW9uIiwiQnJhbmNoT2ZmaWNlc0VkaXRQZXJtaXNzaW9uIiwiQnJhbmNoT2ZmaWNlc0RlbGV0ZVBlcm1pc3Npb24iLCJCcmFuY2hPZmZpY2VzVmlld1Blcm1pc3Npb24iLCJUZWNobmljYWxMb2NhdGlvbkFkZFBlcm1pc3Npb24iLCJUZWNobmljYWxMb2NhdGlvbkVkaXRQZXJtaXNzaW9uIiwiVGVjaG5pY2FsTG9jYXRpb25EZWxldGVQZXJtaXNzaW9uIiwiVGVjaG5pY2FsTG9jYXRpb25WaWV3UGVybWlzc2lvbiJdLCJuYmYiOjE1ODEwMDIwOTQsImV4cCI6MTU4MTAxNDA5NCwiaWF0IjoxNTgxMDAyMDk0LCJpc3MiOiJodHRwczovL2Rldi5pZ2F0LmNsOjIwMDgwIiwiYXVkIjoiaHR0cHM6Ly9kZXYuaWdhdC5jbDoyMDA4MCJ9.M1jUbhI5lNkSbRvi07xYDisEhW_GfJu-0xOFfZv6p-0");
            IRestResponse response = client.Execute(request);
            List<IgoAsset> assets = Newtonsoft.Json.JsonConvert.DeserializeObject<List<IgoAsset>>(response.Content);
            SECTORES sector = db.SECTORES.Where(s => s.SECTOR == codSector).First();

            SetActivos set = new SetActivos();
            set.Activos = new List<Models.UbicacionActivo>();
            foreach (WKB_UBICACION ubicacion in db.WKB_UBICACION.Where(u => u.SECTOR== codSector).ToList())
            {
                //set.Nivel = ubicacion.NIVEL;
                set.Sector = sector.SECTOR;
                UbicacionActivo activo = new UbicacionActivo();
                IgoAsset asset = assets.Where(h => h.AssetID == ubicacion.ID_ACTIVO).First();
                activo.Descripcion = asset.NameAssets;
                activo.Estado = ubicacion.ESTADO.Value;
                activo.IdDiseno = ubicacion.ID_DISENO.Value;
                activo.IdActivo = asset.AssetID;
                activo.Tipo = "0";
                activo.TipoStr = GettipoSTR(0);
                activo.X = ubicacion.UBICACION.XCoordinate.Value;
                activo.Y = ubicacion.UBICACION.YCoordinate.Value;
                activo.Z = 0;
                set.Activos.Add(activo);

            }
            set.Cota = sector.COTA;
            set.Id = sector.ID;
            return new JavaScriptSerializer().Serialize(set);
        }

        public virtual string getDisenoSector(string codSector)
        {
            SetDisenos set = new Models.SetDisenos();

            foreach (SECTORES sector in db.SECTORES.Where(a => a.SECTOR == codSector).ToList())
            {

                List<DisenoModel> lstDisenos = new List<DisenoModel>();
                double X = 0;
                double Y = 0;
                //double Z = 0;
                foreach (DISENOS d in db.DISENOS.Where(a => a.SECTOR == sector.SECTOR).ToList())
                {
                    DisenoModel diseno = new DisenoModel();
                    diseno.DESCRIPCION = d.DESCRIPCION;
                    diseno.id = d.id;
                    diseno.NIVEL = d.NIVEL;
                    diseno.SECTOR = d.SECTOR;
                    diseno.text = d.text;
                    diseno.Ubicacion_PL = d.Ubicacion_PL;
                    diseno.Ubicacion_SC = d.Ubicacion_SC;
                    diseno.Ubicacion_UB = d.Ubicacion_UB;
                    diseno.Centro = new Models.Point3D(d.WKT_DATA.Envelope.Centroid.XCoordinate.Value, d.WKT_DATA.Envelope.Centroid.YCoordinate.Value, sector.COTA);
                    diseno.WKT_DATA = d.WKT_DATA.AsText();
                    X = X + d.WKT_DATA.Envelope.Centroid.XCoordinate.Value;
                    Y = Y + d.WKT_DATA.Envelope.Centroid.YCoordinate.Value;
                    lstDisenos.Add(diseno);
                }

                set.Selected = sector.SECTOR.Equals(codSector);
                set.Id = sector.ID;
                set.Descripcion = sector.DESCRIPCION;
                set.Color = sector.COLOR;
                set.Cota = sector.COTA;
                set.width = sector.LINE_WIDTH.Value;
                set.Disenos = lstDisenos;
                set.Centro = new Point3D() { X = (X / lstDisenos.Count), Y = (Y / lstDisenos.Count) };


            }
            return new JavaScriptSerializer().Serialize(set);
        }


        private JsTreeModel[] GetTreeData()
        {
            var tree = new JsTreeModel[]
            {
                new JsTreeModel { data = "Confirm Application", attr = new JsTreeAttribute { id = "10", selected = true } },
                new JsTreeModel
                {
                    data = "Things",
                    attr = new JsTreeAttribute { id = "20" },
                    children = new JsTreeModel[]
                        {
                            new JsTreeModel { data = "Thing 1", attr = new JsTreeAttribute { id = "21", selected = true } },
                            new JsTreeModel { data = "Thing 2", attr = new JsTreeAttribute { id = "22" } },
                            new JsTreeModel { data = "Thing 3", attr = new JsTreeAttribute { id = "23" } },
                            new JsTreeModel
                            {
                                data = "Thing 4",
                                attr = new JsTreeAttribute { id = "24" },
                                children = new JsTreeModel[]
                                {
                                    new JsTreeModel { data = "Thing 4.1", attr = new JsTreeAttribute { id = "241" } },
                                    new JsTreeModel { data = "Thing 4.2", attr = new JsTreeAttribute { id = "242" } },
                                    new JsTreeModel { data = "Thing 4.3", attr = new JsTreeAttribute { id = "243" } }
                                },
                            },
                        }
                },
                new JsTreeModel
                {
                    data = "Colors",
                    attr = new JsTreeAttribute { id = "40" },
                    children = new JsTreeModel[]
                        {
                            new JsTreeModel { data = "Red", attr = new JsTreeAttribute { id = "41" } },
                            new JsTreeModel { data = "Green", attr = new JsTreeAttribute { id = "42" } },
                            new JsTreeModel { data = "Blue", attr = new JsTreeAttribute { id = "43" } },
                            new JsTreeModel { data = "Yellow", attr = new JsTreeAttribute { id = "44" } },
                        }
                }
            };

            return tree;
        }
    }
}