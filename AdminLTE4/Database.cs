﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using System.Web;

namespace IGO_DEMO
{
    public class Database
    {
        private SqlConnection _connection;

        public Database(string stringConnection)
        {
            _connection = new SqlConnection(stringConnection);
            _connection.Open();
        }
        public bool IsConnected()
        {
            if (_connection != null)
                return (_connection.State == ConnectionState.Open);
            else
                return false;
        }


        //public Dictionary<string, OBJInfo> getOBJInfo()
        //{
        //}
    }
}