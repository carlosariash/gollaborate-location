﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGO_DEMO.Models
{
    public class DisenoModel
    {
        public int id { get; set; }
        public string WKT_DATA { get; set; }
        public Point3D Centro { get; set; }
        public string SECTOR { get; set; }
        public string NIVEL { get; set; }
        public string DESCRIPCION { get; set; }
        public string text { get; set; }
        public string Ubicacion_PL { get; set; }
        public string Ubicacion_SC { get; set; }
        public string Ubicacion_UB { get; set; }
    }
}