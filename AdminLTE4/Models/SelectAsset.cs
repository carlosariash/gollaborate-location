﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace IGO_DEMO.Models
{
    public class SelectAsset
    {
        public List<SelectListItem> AssetTypes { get; set; }
        public List<IgoAsset> assets { get; set; }
    }
}