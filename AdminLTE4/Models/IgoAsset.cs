﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGO_DEMO.Models
{
    public class IgoAsset
    {
        public int AssetID { get; set; }
        public string TypeAssets { get; set; }
        public string NameAssets { get; set; }
        public bool Unsubscribe { get; set; }
        public DateTime CreationDate { get; set; }
        public string BranchOffice { get; set; }
        public string NumberInternal { get; set; }
        public string Model { get; set; }
        public string NumberSerie { get; set; }
        public string SubTypeAssets { get; set; }
        public string BranchOfficeId { get; set; }
        public string NameCompound { get; set; }
    }
}