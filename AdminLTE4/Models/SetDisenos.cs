﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGO_DEMO.Models
{
    public class Point3D
    {
        public Point3D(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }
        public Point3D()
        {

        }
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
    }

    public class SetsDisenos
    {
        public string Descripcion { get; set; }
        public Point3D Centro { get; set; }

        public Double Cota0 { get; set; }
        public List<SetDisenos> Sets { get; set; }
    }

    public class UbicacionActivo
    {
        public int IdActivo { get; set; }
        public int IdDiseno { get; set; }
        public string Descripcion { get; set; }
        public string Tipo { get; set; }
        public string TipoStr { get; set; }
        public int Estado { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }

    }
    public class SetActivos
    {

        public int Id { get; set; }
        public string Sector { get; set; }
        public string Nivel { get; set; }
        public double Cota { get; set; }
        public List<UbicacionActivo> Activos { get; set; }
    }

    public class SetDisenos
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public double Cota { get; set; }
        public Point3D Centro { get; set; }
        public List<DisenoModel> Disenos { get; set; }
        public Boolean Selected { get; set; }
        public int Color { get; set; }
        public int width { get; set; }


    }
}